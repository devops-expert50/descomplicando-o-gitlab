# Descomplicando o Gitlab

Treinamento Descomplicando o Gitlab >> https://www.youtube.com/watch?v=SMzaAP09BD4&ab_channel=LINUXtips


## Day-1
```bash
 - Entendemos o que e o Git
 - Entendemos o que e o Gitlab
 - Aprendemos os comandos basicos para manipulacao de arquivos e diretorios no git
 - Como criar uma branch
 - Como criar um Merge Request
 - Como criar um repositorio Git
 - Como adicionar um membro no projeto
 - Como criar um grupo no Gitlab
 - Como fazer o merge na master
```
